/**
 * @author      : oli (oli@oli-HP)
 * @file        : main
 * @created     : Wednesday Nov 16, 2022 17:54:04 GMT
 */

#include <iostream>

#include "matrix.hpp"
#include "optical_components.hpp"
#include "state.hpp"

int main()
{
    // olistd::Matrixcd b = olistd::Coupler{0.5};
    // std::cout << b;
    // std::cout << b * b.H();

    // olistd::Matrixcd phase = olistd::Phase{M_PI/ 4.0};
    // std::cout << phase;

    olistd::Phase ps{M_PI/4.0, 2};
    olistd::Coupler<double> bs{0.5, 0, 1};
    std::cout << bs.matrix();
    std::cout << bs.matrix() * bs.matrix().H();

    olistd::Transform m = ps * bs;
    std::cout << m.matrix();

    olistd::Vectorcd v(3);
    v(0) = 1.0;
    std::cout << "m*v\n" << m * v;

    olistd::Transform t{3};
    std::cout << "t\n" << t.matrix();
    t *= olistd::Coupler{0.4, 1,2};
    std::cout << "t\n" << t.matrix();
    t *= olistd::Phase{M_PI/4.0, 1};
    std::cout << "t\n" << t.matrix();

    std::cout << "t*v\n" << t * v;

    auto thing = t*v;

    std::cout << "thing \n" << thing; 


    v *= olistd::Coupler{0.2, 0, 1}.matrix(v.size());
    std::cout << "v\n" << v;


    olistd::State<double> state;
    state << olistd::Coupler{0.5, 0, 1};
    state << olistd::Coupler{0.4, 1, 3};
    state << olistd::Coupler{0.5, 3, 4};

    state.vector = olistd::Vectorcd{{0.0, 1.0, 0.0, 1.0, 0.0, 1.0}};
    state.evolve();

    return 0;
}
