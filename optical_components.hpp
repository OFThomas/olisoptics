/**
 * @author      : oli (oli@oli-HP)
 * @file        : optical_components
 * @created     : Thursday Nov 17, 2022 17:39:41 GMT
 */

#ifndef OPTICAL_COMPONENTS_HPP
#define OPTICAL_COMPONENTS_HPP

// #include <olistd/flat_map>
// #include <olistd/io>

#include "matrix.hpp"

#include <string>

#include <algorithm>
#include <vector>

namespace olistd
{
    template <typename Scalar = double>
        class Transform
        {
            private:
                using CScalar = std::complex<Scalar>;
                std::vector<std::pair<std::string, Scalar>> params;
                olistd::Matrix<CScalar> mat;
                std::vector<size_t> modes;
            public:

                Transform(size_t size) : mat(olistd::Matrix<CScalar>::Identity(size,size)), modes(olistd::lin_vec<size_t>(size))
            {}

                Transform(const std::vector<std::pair<std::string, Scalar>> & p,
                        const olistd::Matrix<CScalar> & m,
                        const std::vector<size_t> & modes = {})
                    : params(p), mat(m), modes(modes)
                    {}

                constexpr const std::map<std::string, Scalar> & get_params() const
                {
                    return params;
                }

                operator olistd::Matrix<CScalar>() const
                {
                    size_t max_modes = *std::max_element(modes.begin(), modes.end());
                    std::vector<size_t> full_modes = olistd::lin_vec<size_t>(max_modes);
                    auto diff = olistd::vector_difference(full_modes, this->modes);
                    size_t newdim = this->modes.size() + diff.size();

                    Matrix<CScalar> lhs_mat = Matrix<CScalar>::Identity(newdim);
                    lhs_mat.set(this->modes, this->matrix());

                    return lhs_mat;
                }

                constexpr const olistd::Matrix<CScalar> & matrix() const
                {
                    return mat;
                }

                olistd::Matrix<CScalar> matrix(size_t vsize) const
                {
                    size_t max_modes = *std::max_element(modes.begin(), modes.end());
                    if(vsize > max_modes)
                        max_modes = vsize;
                    std::vector<size_t> full_modes = olistd::lin_vec<size_t>(max_modes);
                    auto diff = olistd::vector_difference(full_modes, this->modes);
                    size_t newdim = this->modes.size() + diff.size();

                    Matrix<CScalar> lhs_mat = Matrix<CScalar>::Identity(newdim);
                    lhs_mat.set(this->modes, this->matrix());

                    return lhs_mat;
                }

                Transform & operator*=(const Transform & rhs)
                {
                    // given the matrix of _this_ we check which modes
                    //  _this_ is over 
                    //
                    //  case 1: _this_ and _rhs_ act on the same modes,  in the 
                    //  same order, 
                    //      just do matrix multiplication
                    //
                    //  case 2: _this_ and _rhs_ act on completely different
                    //  modes, 
                    //      pad (direct sum identities to _this_ then multiply 
                    //      the correct sub blocks with _rhs_
                    //
                    //  case 3: _this_ and _rhs_ overlap on some modes but not all
                    //      -> pad _this_ again then multiply subblocks 
                    auto diff = olistd::vector_difference(rhs.modes, this->modes);

                    // std::cout << "lhs modes ";
                    // olistd::print(this->modes);
                    // std::cout << "rhs modes ";
                    // olistd::print(rhs.modes);
                    // std::cout << "diff modes ";
                    // olistd::print(diff);

                    size_t newdim = this->modes.size() + diff.size();

                    std::vector<size_t> uni = olistd::vector_union(this->modes, rhs.modes);
                    // std::cout << "union ";
                    // olistd::print(uni);

                    std::vector<size_t> leftidx = olistd::vec_pos_in(this->modes, uni);
                    std::vector<size_t> rightidx = olistd::vec_pos_in(rhs.modes, uni);

                    // std::cout << "leftidx ";
                    // olistd::print(leftidx);
                    // std::cout << "rightidx ";
                    // olistd::print(rightidx);

                    Matrix<CScalar> lhs_mat = Matrix<CScalar>::Identity(newdim);
                    lhs_mat.set(leftidx, this->matrix());

                    Matrix<CScalar> rhs_mat = Matrix<CScalar>::Identity(newdim);
                    rhs_mat.set(rightidx, rhs.matrix());

                    // std::cout << "this->matrix()\n" << this->matrix();
                    // std::cout << "rhs.matrix()\n" << rhs.matrix();

                    // std::cout << "lhs\n" << lhs_mat;
                    // std::cout << "rhs\n" << rhs_mat;
                    // std::cout << "lhs * rhs\n" << lhs_mat * rhs_mat;

                    mat = lhs_mat * rhs_mat;
                    return *this;
                }

                friend Transform operator*(Transform lhs, const Transform & rhs)
                {
                    lhs *= rhs;
                    return lhs;
                }

                friend olistd::Vector<CScalar> operator*(const Transform & lhs, const Vector<CScalar> & v) 
                {
                    Transform t{v.size()};
                    t *= lhs;

                    return t.matrix() * v;
                }

                // friend Vector<Scalar> operator*=(const Transform & transform)
                // {

                // }
        };

    template <typename Scalar>
        class Coupler : public olistd::Transform<Scalar>
    {
        private:
            olistd::Matrix<std::complex<Scalar>> make_mat(Scalar reflectivity)
            {
                Scalar r = std::sqrt(reflectivity);
                Scalar t = std::sqrt(1-reflectivity);
                olistd::Matrix<std::complex<Scalar>> mat = 
                {{t, std::complex<Scalar>{0.0, 1.0} * r,
                     std::complex<Scalar>{0.0, 1.0} * r, t}};
                return mat;
            }

        public:
            Coupler(const Scalar & reflectivity) : 
                Transform<Scalar>(
                        {std::make_pair("Reflectivity", reflectivity)}, 
                        make_mat(reflectivity))
                {}

            Coupler(const Scalar & reflectivity, size_t mode1, size_t mode2) : 
                Transform<Scalar>(
                        {std::make_pair("Reflectivity", reflectivity)}, 
                        make_mat(reflectivity), 
                        {mode1, mode2})
            {}
    };

    template <typename Scalar>
        class Phase : public olistd::Transform<Scalar>
    {
        private:
            olistd::Matrix<std::complex<Scalar>> make_mat(Scalar phase)
            {
                std::complex<Scalar> theta = std::exp(phase * std::complex<Scalar>{0.0,1.0});
                olistd::Matrix<std::complex<Scalar>> mat = {{theta}};
                return mat;
            }

        public:
            Phase(const Scalar & phase) :  
                Transform<Scalar>(
                        {std::make_pair("Phase", phase)}, 
                        make_mat(phase))
                {}
            Phase(const Scalar & phase, size_t mode) : 
                Transform<Scalar>(
                        {std::make_pair("Phase", phase)}, 
                        make_mat(phase),
                        {mode})
            {}

    };




}


#endif // end of include guard OPTICAL_COMPONENTS_HPP 

