/**
 * @author      : oli (oli@oli-HP)
 * @file        : matrix
 * @created     : Wednesday Nov 16, 2022 17:44:17 GMScalar
 */

#ifndef MAScalarRIX_HPP
#define MAScalarRIX_HPP

#include <olistd/io>

#include <vector>

namespace olistd 
{
    template <typename Scalar>
        class Matrix
        {
            private:
                std::size_t num_rows;
                std::size_t num_cols;
                std::vector<Scalar> mdata;

            public:

                Matrix() = default;
                Matrix(size_t rows, size_t cols) : 
                    Matrix(rows, cols, std::vector<Scalar>(rows*cols, static_cast<Scalar>(0.0)))
            {}

                Matrix(size_t rows, size_t cols, const std::vector<Scalar> & mdata) 
                    : num_rows(rows), num_cols(cols), mdata(mdata)
                {}

                Matrix(const std::vector<Scalar> & mdata) :
                    Matrix( std::sqrt(mdata.size()), mdata.size()/std::sqrt(mdata.size()), mdata)
            {}

                constexpr operator Scalar() const 
                {
                    assert(num_rows == 1 && "rows != 1");
                    assert(num_cols == 1 && "cols != 1");
                    assert(mdata.size() == 1 && "mdata.size() != 1");
                    return mdata[0];
                }

                constexpr Scalar operator()(size_t row, size_t col) const
                {
                    return mdata[row*num_cols + col];
                }

                Scalar & operator()(size_t row, size_t col)
                {
                    return mdata[row*num_cols + col];
                }

                constexpr std::size_t rows() const
                {
                    return num_rows;
                }

                constexpr std::size_t cols() const
                {
                    return num_cols;
                }

                std::string print() const
                {
                    std::stringstream stream;
                    for(size_t i = 0; i < rows(); i++)
                    {
                        stream << "[";
                        for(size_t j = 0; j < cols()-1; j++)
                            stream << this->operator()(i,j) << ", ";
                        stream << this->operator()(i, cols()-1);
                        stream << "]\n";
                    }
                    return stream.str();
                }
                friend std::ostream & operator<<(std::ostream & os, const Matrix & matrix)
                {
                    os << matrix.print();
                    return os;
                }

                const std::vector<Scalar> & data() const
                {
                    return mdata;
                }

                void set(const std::vector<size_t> & idx, const Matrix & mat_in)
                {
                    for(size_t r = 0; r < idx.size(); r++)
                        for(size_t c = 0; c < idx.size(); c++)
                            this->operator()(idx[r], idx[c]) = mat_in(r, c); 
                }

                Matrix get(const std::vector<size_t> & idx) const
                {
                    Matrix out(idx.size(), idx.size());
                    for(size_t r = 0; r < idx.size(); r++)
                        for(size_t c = 0; c < idx.size(); c++)
                            out(r, c) = this->operator()(idx[r], idx[c]); 
                }

                Matrix & transpose()
                {
                    std::size_t old_rows = num_rows;
                    std::size_t old_cols = num_cols;
                    num_rows = old_cols;
                    num_cols = old_rows;

                    std::vector<Scalar> old_mdata(mdata);

                    for(std::size_t row = 0; row < old_rows; row++)
                    {
                        for(std::size_t col = 0; col < old_cols; col++)
                        {
                            this->operator()(col, row) = old_mdata[row * old_cols + col];
                        }
                    }
                    return *this;
                }

                Matrix transposeinplace() const
                {
                    Matrix mcopy(*this);
                    mcopy.transpose();
                    return mcopy;
                }
                Matrix T() const
                {
                    return transposeinplace();
                }
                Matrix & adjoint()
                {
                    std::size_t old_rows = num_rows;
                    std::size_t old_cols = num_cols;
                    num_rows = old_cols;
                    num_cols = old_rows;

                    std::vector<Scalar> old_mdata(mdata);

                    for(std::size_t row = 0; row < old_rows; row++)
                    {
                        for(std::size_t col = 0; col < old_cols; col++)
                        {
                            this->operator()(col, row) = std::conj(old_mdata[row * old_cols + col]);
                        }
                    }
                    return *this;
                }

                Matrix adjointinplace() const
                {
                    Matrix mcopy(*this);
                    mcopy.adjoint();
                    return mcopy;
                }
                Matrix H() const
                {
                    return adjointinplace();
                }

                // ---------------------------------------------------------------------
                // Arithmetic operations 
                Matrix & operator+=(const Matrix & rhs)
                {
                    assert(this->rows() == rhs.rows() && "Err in += rows not equal");
                    assert(this->cols() == rhs.cols() && "Err in += cols not equal");
                    for(std::size_t row = 0; row < this->rows(); row++)
                        for(std::size_t col = 0; col < this->cols(); col++)
                            this->operator()(row, col) += rhs(row,col);

                    return *this;
                }
                friend Matrix operator+(Matrix lhs, const Matrix & rhs)
                {
                    lhs += rhs;
                    return lhs;
                }

                Matrix & operator-=(const Matrix & rhs)
                {
                    assert(this->rows() == rhs.rows() && "Err in += rows not equal");
                    assert(this->cols() == rhs.cols() && "Err in += cols not equal");
                    for(std::size_t row = 0; row < this->rows(); row++)
                        for(std::size_t col = 0; col < this->cols(); col++)
                            this->operator()(row, col) -= rhs(row,col);

                    return *this;
                }
                friend Matrix operator-(Matrix lhs, const Matrix & rhs)
                {
                    lhs -= rhs;
                    return lhs;
                }
                Matrix & operator-()
                {
                    this->operator*=(static_cast<Scalar>(-1.0));
                    return *this;
                }

                Matrix & operator*=(const Matrix & rhs)
                {
                    // [A, B]  * [E, F]  =  [ A*E + B*G, A*F + B*H ] 
                    // [C, D]    [G, H]     [ C*E + D*G, C*F + D*H ]
                    assert(this->cols() == rhs.rows() && "Err in *= row, cols not equal");

                    Matrix lhs(*this);

                    for(std::size_t row = 0; row < lhs.rows(); row++)
                    {
                        for(std::size_t col = 0; col < lhs.cols(); col++)
                        {
                            // row, col of output = 
                            this->operator()(row, col)= static_cast<Scalar>(0.0);
                            for(std::size_t k = 0; k < lhs.cols(); k++)
                                this->operator()(row, col) += lhs(row, k) * rhs(k, col);
                        }
                    }
                    return *this;
                }
                friend Matrix operator*(Matrix lhs, const Matrix & rhs)
                {
                    lhs *= rhs;
                    return lhs;
                }

                Matrix & operator*=(const Scalar & rhs)
                {
                    for(std::size_t row = 0; row < this->rows(); row++)
                        for(std::size_t col = 0; col < this->cols(); col++)
                            this->operator()(row, col) *= rhs;

                    return *this;
                }
                friend Matrix operator*(Matrix lhs, const Scalar & rhs)
                {
                    lhs *= rhs;
                    return lhs;
                }
                friend Matrix operator*(const Scalar & lhs, Matrix rhs)
                {
                    rhs *= lhs;
                    return rhs;
                }


                ///// --------------------------------------------------------
                // Static matrices, useful for assignments etc
                static constexpr Matrix<Scalar> Constant(size_t r, size_t c, Scalar val)
                {
                    std::vector<Scalar> d(r * c, val);
                    return Matrix<Scalar>{r, c, d};
                }
                static constexpr Matrix<Scalar> Constant(size_t n, Scalar val)
                {
                    return Constant(n, n, val);
                }

                static constexpr Matrix<Scalar> Identity(size_t r, size_t c)
                {
                    std::vector<Scalar> d(r * c, static_cast<Scalar>(0.0));
                    Matrix<Scalar> m{r, c, d};
                    for(size_t row = 0; row < r; row++)
                        m(row, row) = static_cast<Scalar>(1.0);
                    return m;
                }
                static constexpr Matrix<Scalar> Identity(size_t n)
                {
                    return Identity(n,n);
                }

                static constexpr Matrix<Scalar> Zeros(size_t r, size_t c)
                {
                    std::vector<Scalar> d(r * c, static_cast<Scalar>(0.0));
                    return Matrix<Scalar>{r, c, d};
                }
                static constexpr Matrix<Scalar> Zeros(size_t n)
                {
                    return Zeros(n,n);
                }

                static constexpr Matrix<Scalar> Swap(size_t r, size_t c)
                {
                    std::vector<Scalar> d(r * c, static_cast<Scalar>(0.0));
                    Matrix<Scalar> m{r, c, d};
                    for(size_t row = 0; row < r-1; row++)
                    {
                        m(row, row+1) = static_cast<Scalar>(1.0);
                        m(row+1, row) = static_cast<Scalar>(1.0);
                    }
                    return m;
                }
                static constexpr Matrix<Scalar> Swap(size_t n)
                {
                    return Swap(n,n);
                }

        };

    template <typename Scalar>
        class Vector
        {
            private:
                std::size_t vsize;
                std::vector<Scalar> vdata;
                bool col_vec = true;

            public:

                Vector() = default;
                Vector(size_t vsize, const std::vector<Scalar> & vdata) : vsize(vsize), vdata(vdata) {}
                Vector(size_t vsize) : Vector(vsize, std::vector<Scalar>(vsize, static_cast<Scalar>(0.0))) {}
                Vector(const std::vector<Scalar> & vdata) : Vector(vdata.size(), vdata) {}

                // template <typename Y>
               // Vector(std::initializer_list<Y> list) : Vector(std::vector<Scalar>(list)) {}

                constexpr Scalar operator()(size_t idx) const
                {
                    return vdata[idx];
                }

                Scalar & operator()(size_t idx)
                {
                    return vdata[idx];
                }

                constexpr std::size_t rows() const
                {
                    return (col_vec) ? size() : 1;
                }

                constexpr std::size_t cols() const
                {
                    return (col_vec) ? 1 : size();
                }

                constexpr std::size_t size() const
                {
                    return vsize;
                }

                std::string print_inline() const
                {
                    std::stringstream stream;
                    stream << "{";
                    for(size_t i = 0; i < size()-1; i++)
                        stream << vdata[i] << ", ";
                    stream << vdata[size() - 1] << "}";

                    if(col_vec)
                        stream << "^Scalar\n";
                    else
                        stream << "\n";
                    return stream.str();
                }

                std::string print() const
                {
                    std::stringstream stream;
                    if(is_rowvec())
                    {
                        stream << "{";
                        for(size_t i = 0; i < size()-1; i++)
                            stream << vdata[i] << ", ";
                        stream << vdata[size() - 1] << "}\n";
                    }
                    else // is_colvec
                        for(size_t i = 0; i < size(); i++)
                            stream << "{" << vdata[i] << "}\n";

                    return stream.str();
                }

                friend std::ostream & operator<<(std::ostream & os, const Vector & vec)
                {
                    os << vec.print();
                    return os;
                }


                constexpr bool is_colvec() const 
                {
                    return col_vec;
                }

                constexpr bool is_rowvec() const
                {
                    return not col_vec;
                }

                Vector & transpose()
                {
                    col_vec = not col_vec;
                    return *this;
                }
                Vector transposeinplace() const
                {
                    Vector mcopy(*this);
                    mcopy.transpose();
                    return mcopy;
                }
                Vector T() const 
                {
                    return transposeinplace();
                }

                Vector & adjoint()
                {
                    col_vec = not col_vec;
                    for(size_t i = 0; i < size(); i++)
                        this->operator()(i) = std::conj(this->operator()(i));
                    return *this;
                }
                Vector adjointinplace() const
                {
                    Vector mcopy(*this);
                    mcopy.adjoint();
                    return mcopy;
                }
                Vector H() const 
                {
                    return adjointinplace();
                }


                const std::vector<Scalar> & data() const
                {
                    return vdata;
                }
                // ---------------------------------------------------------------------
                // Arithmetic operations 
                Vector & operator+=(const Vector & rhs)
                {
                    assert(this->size() == rhs.size() && "Err in += size not equal");
                    assert(this->col_vec == rhs.col_vec() && "Err in += vector types not equal");

                    for(std::size_t i = 0; i < this->size(); i++)
                        this->operator()(i) += rhs(i);

                    return *this;
                }
                friend Vector operator+(Vector lhs, const Vector & rhs)
                {
                    lhs += rhs;
                    return lhs;
                }

                Vector & operator-=(const Vector & rhs)
                {
                    assert(this->size() == rhs.size() && "Err in -= size not equal");
                    assert(this->col_vec == rhs.col_vec && "Err in -= vector types not equal");
                    for(std::size_t i = 0; i < this->size(); i++)
                        this->operator()(i) -= rhs(i);

                    return *this;
                }
                friend Vector operator-(Vector lhs, const Vector & rhs)
                {
                    lhs -= rhs;
                    return lhs;
                }
                Vector & operator-()
                {
                    this->operator*=(static_cast<Scalar>(-1.0));
                    return *this;
                }

                Vector & operator*=(const Matrix<Scalar> & rhs)
                {
                    // [A, B]  * [E]  =  [ A*E + B*G] 
                    // [C, D]    [G]     [ C*E + D*G]
                    assert(this->rows() == rhs.cols() && "Err in *= row, cols not equal");

                    Vector lhs(*this);

                    for(std::size_t row = 0; row < lhs.rows(); row++)
                    {
                        // row, col of output = 
                        this->operator()(row)= static_cast<Scalar>(0.0);
                        for(std::size_t k = 0; k < lhs.rows(); k++)
                            this->operator()(row) += rhs(row, k) * lhs(k);
                    }
                    return *this;
                }
                friend Vector operator*(const Matrix<Scalar> & rhs, Vector lhs)
                {
                    lhs *= rhs;
                    return lhs;
                }
                Vector & operator*=(const Scalar & rhs)
                {
                    for(std::size_t size = 0; size < this->size(); size++)
                        this->operator()(size) *= rhs;

                    return *this;
                }
                friend Vector operator*(Vector lhs, const Scalar & rhs)
                {
                    lhs *= rhs;
                    return lhs;
                }
                friend Vector operator*(const Scalar & lhs, Vector rhs)
                {
                    rhs *= lhs;
                    return rhs;
                }

                friend Matrix<Scalar> operator*(Vector lhs, const Vector & rhs)
                {
                    if(lhs.is_rowvec() && rhs.is_colvec())
                    {
                        assert(lhs.is_rowvec() && "lhs not row vec");
                        assert(rhs.is_colvec() && "rhs not col vec");

                        Matrix<Scalar> sum{1,1,{static_cast<Scalar>(0.0)}};
                        for(size_t i = 0; i < lhs.size(); i++)
                            sum(0,0) += lhs(i) * rhs(i);
                        return sum;
                    }
                    else if(lhs.is_colvec() && rhs.is_rowvec())
                    {
                        Matrix<Scalar> mat(lhs.size(), rhs.size());
                        for(size_t r = 0; r < lhs.size(); r++)
                            for(size_t c = 0; c < rhs.size(); c++)
                                mat(r, c) = lhs(r) * rhs(c);
                        return mat;
                    }
                    else 
                    {
                        std::cerr << "err not valid vector vector product" << std::endl;
                        abort();
                    }
                }
                friend Vector operator*(Vector lhs, const Matrix<Scalar> & rhs)
                {
                    assert(lhs.is_rowvec() && "lhs not rowvec");
                    assert(lhs.cols() == rhs.rows() && "lhs cols != rhs.rows");

                    Vector<Scalar> v(lhs.cols());
                    for(size_t c = 0; c < v.size(); c++)
                        for(size_t k = 0; k < lhs.cols(); k++)
                            v(c) += lhs(k)*rhs(k, c);
                    v.transpose();
                    return v;
                }

                Vector & operator/=(Scalar rhs)
                {
                    for(size_t n = 0; n < size(); n++)
                        this->operator()(n) /= rhs;
                    return *this;
                }
                friend Vector operator/(Vector lhs, Scalar rhs)
                {
                    lhs /= rhs;
                    return lhs;
                }

                ///// --------------------------------------------------------
                // Static matrices, useful for assignments etc
                static constexpr Vector<Scalar> Constant(size_t n, Scalar val)
                {
                    std::vector<Scalar> d(n, val);
                    return Vector<Scalar>{n, d};
                }

                static constexpr Vector<Scalar> Zeros(size_t n)
                {
                    std::vector<Scalar> d(n, static_cast<Scalar>(0.0));
                    return Vector<Scalar>{n, d};
                }
        };


    // using Matrixd = Matrix<double>;
    typedef Matrix<double> Matrixd;
    typedef Matrix<float> Matrixf;
    typedef Matrix<std::complex<double>> Matrixcd;
    typedef Matrix<std::complex<float>> Matrixcf;

    typedef Vector<double> Vectord;
    typedef Vector<float> Vectorf;
    typedef Vector<std::complex<double>> Vectorcd;
    typedef Vector<std::complex<float>> Vectorcf;

}

#endif // end of include guard MAScalarRIX_HPP 

