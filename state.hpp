/**
 * @author      : oli (oli@oli-HP)
 * @file        : state
 * @created     : Thursday Nov 17, 2022 22:19:02 GMT
 */

#ifndef STATE_HPP
#define STATE_HPP

#include "optical_components.hpp"

namespace olistd
{

    template <typename Scalar>
        class State
        {
            private:
                using Cscalar = std::complex<Scalar>;
                std::vector<Transform<Scalar>> transforms;
                std::vector<olistd::Vector<Cscalar>> saved_states;

            public:
                State() = default;

                olistd::Vector<Cscalar> vector;

                void operator<<(const Transform<Scalar> & transform)
                {
                    transforms.emplace_back(transform);
                }

                void evolve()
                {
                    std::cout << "vector\n" << vector;
                    saved_states = {vector};
                    for(const auto & transform : transforms)
                    {
                        // vector = transform * vector;
                        vector *= transform.matrix(vector.size());
                        saved_states.push_back(vector);
                        std::cout << "vector\n" << vector;
                    }
                }




        };

}
#endif // end of include guard STATE_HPP 

