/**
 * @author      : oli (oli@oli-HP)
 * @file        : unit_tests
 * @created     : Thursday Nov 17, 2022 15:21:05 GMT
 */
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include "matrix.hpp"

TEST_CASE("matrix ctor")
{
    const std::vector<float> nums = {1.0,2.0, 3.0,4.0};
    SUBCASE("Matrix vector floats")
    {
        olistd::Matrix matrix{2, 2, nums};
        // std::cout << matrix;
        REQUIRE(matrix(0,0) == nums[0]);
        REQUIRE(matrix(0,1) == nums[1]);
        REQUIRE(matrix(1,0) == nums[2]);
        REQUIRE(matrix(1,1) == nums[3]);
    }

    SUBCASE("MATRIX ASSINGMENT OPERATOR")
    {
        olistd::Matrixf matrix = olistd::Matrix{nums};
        REQUIRE(matrix(0,0) == nums[0]);
        REQUIRE(matrix(0,1) == nums[1]);
        REQUIRE(matrix(1,0) == nums[2]);
        REQUIRE(matrix(1,1) == nums[3]);
        // std::cout << matrix;
    }

    SUBCASE("MATRIX TRANSPOSE")
    {
        olistd::Matrixf matrix{nums};
        REQUIRE(matrix(0,0) == nums[0]);
        REQUIRE(matrix(0,1) == nums[1]);
        REQUIRE(matrix(1,0) == nums[2]);
        REQUIRE(matrix(1,1) == nums[3]);

        matrix.transposeinplace();
        // check elements unchanged 
        REQUIRE(matrix(0,0) == nums[0]);
        REQUIRE(matrix(0,1) == nums[1]);
        REQUIRE(matrix(1,0) == nums[2]);
        REQUIRE(matrix(1,1) == nums[3]);

        matrix.transpose();
        REQUIRE(matrix(0,0) == nums[0]);
        REQUIRE(matrix(1,0) == nums[1]);
        REQUIRE(matrix(0,1) == nums[2]);
        REQUIRE(matrix(1,1) == nums[3]);
    }

    SUBCASE("Constant matrix ctor")
    {
        double value = 3.0;
        olistd::Matrix matrix = olistd::Matrixd::Constant(2, value);
        REQUIRE(matrix(0,0) == value);
        REQUIRE(matrix(0,1) == value);
        REQUIRE(matrix(1,0) == value);
        REQUIRE(matrix(1,1) == value);
    }

    SUBCASE("Identity matrix ctor")
    {
        olistd::Matrixd matrix = olistd::Matrixd::Identity(2);
        REQUIRE(matrix(0,0) == 1.0);
        REQUIRE(matrix(0,1) == 0.0);
        REQUIRE(matrix(1,0) == 0.0);
        REQUIRE(matrix(1,1) == 1.0);
    }

    SUBCASE("Zero matrix ctor")
    {
        olistd::Matrixd matrix = olistd::Matrixd::Zeros(2);
        REQUIRE(matrix(0,0) == 0.0);
        REQUIRE(matrix(0,1) == 0.0);
        REQUIRE(matrix(1,0) == 0.0);
        REQUIRE(matrix(1,1) == 0.0);
    }

    SUBCASE("Matrix swap ctor")
    {
        olistd::Matrix matrix = olistd::Matrixd::Swap(2);
        REQUIRE(matrix(0,0) == 0.0);
        REQUIRE(matrix(0,1) == 1.0);
        REQUIRE(matrix(1,0) == 1.0);
        REQUIRE(matrix(1,1) == 0.0);
    }
}




TEST_CASE("Vectors ctor")
{
    const std::vector<double> nums{1.0, 2.0, 3.0, 4.0};
    SUBCASE("std::vector ctor")
    {
        olistd::Vector v{nums};
        for(size_t i = 0; i < v.size(); i++)
            REQUIRE(v(i) == nums[i]);
    }

    SUBCASE("Vector transpose")
    {
        olistd::Vector v{nums};
        for(size_t i = 0; i < v.size(); i++)
            REQUIRE(v(i) == nums[i]);

        REQUIRE(v.is_colvec() == true);
        REQUIRE(v.is_rowvec() == false);

        v.transposeinplace();
        for(size_t i = 0; i < v.size(); i++)
            REQUIRE(v(i) == nums[i]);
        REQUIRE(v.is_colvec() == true);
        REQUIRE(v.is_rowvec() == false);

        v.transpose();
        for(size_t i = 0; i < v.size(); i++)
            REQUIRE(v(i) == nums[i]);
        REQUIRE(v.is_colvec() == false);
        REQUIRE(v.is_rowvec() == true);
    }
}


TEST_CASE("Matrix arithmetic")
{

    olistd::Matrix ident = olistd::Matrixd::Identity(2);
    olistd::Matrix mat = olistd::Matrixd::Identity(2);
    olistd::Matrix mat1 = olistd::Matrixd::Identity(2);
    mat += mat1;
    std::cout << "mat += mat1\n" << mat;


    mat += olistd::Matrixd::Identity(2);
    std::cout << "mat += Identity\n" << mat;


    std::cout << "ident + ident \n" << ident + ident;
    std::cout << "ident * ident \n" << ident * ident;

    olistd::Matrix m = olistd::Matrixd::Swap(2);
    olistd::Matrix s = olistd::Matrixd::Swap(4);
    std::cout << "swap\n" << m;

    std::cout << "swap * swap \n" << m + m;
    std::cout << "swap * swap \n" << m * m;

    std::cout << "swap 5\n" << s;
    std::cout << "swap5 * swap 5\n" << s*s;

    std::cout << "swap - ident\n" << m - ident;

    std::cout << "m * 2.0\n" << m * 2.0;
    std::cout << "2.0 * m * 2.0\n" << 2.0 * m * 2.0;

    std::cout << "m\n" << m;
    std::cout << "-m\n" << -m;

    olistd::Matrixd l{{0, 1, 0,0 }};
    std::cout <<"l\n" << l;

    olistd::Matrixd r{{0, 0, 1,0 }};
    std::cout <<"r\n" << r;


    std::cout <<"l*r\n" << l*r;
    std::cout <<"r*l\n" << r * l;

    olistd::Vectord ve{{1, 1}};

    std::cout << "ve\n" << ve;
    std::cout << "2.0 * ve\n" << 2.0 * ve;
    std::cout << "ve * 2.0\n" << ve * 2.0;


    std::cout << "ident * ve\n" << 2.0 * ident * ve;

    std::cout << "veT * ve\n" << ve.transposeinplace() * ve;

    std::cout << "ve * veT\n" << ve * ve.transposeinplace();

    double val = ve.transposeinplace() * ve;
    std::cout << "val " << val << std::endl;

    olistd::Vectord myv = 2.0 * olistd::Matrixd::Constant(2,3) * ve;
    std::cout << "myv \n" << myv;

    myv *= 1.0/6.0;
    std::cout << "myvt\n" << myv.transposeinplace();
    std::cout << "ident\n" << ident;
    std::cout << "myv\n" << myv;
    std::cout << "myvT * ident\n" <<  myv.transposeinplace() * ident;
    std::cout << "myvT * ident * myv\n" << myv.transposeinplace() * ident * myv;

    double val33 = myv.transposeinplace() * ident * myv;
    std::cout << "val33 = " << val33 << std::endl;

}
